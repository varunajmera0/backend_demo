from rest_framework import serializers
from .models import University, Student, Payment_Detail, Membership
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from rest_framework.serializers import (EmailField)
from django.db.models import Q
from rest_framework import exceptions
from django.contrib.auth import authenticate
from datetime import date
from dateutil.relativedelta import relativedelta
import re

User = get_user_model()

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        username = data.get("username", "")
        password = data.get("password", "")

        if username and password:
            user = authenticate(email=username, password=password)
            if user:
                if user.is_active:
                    data["user"] = user
                else:
                    msg = "User is deactivated."
                    raise exceptions.ValidationError(msg)
            else:
                msg = "Unable to login with given credentials."
                raise exceptions.ValidationError(msg)
        else:
            msg = "Must provide username and password both."
            raise exceptions.ValidationError(msg)
        return data


class UserCreateSerializer(serializers.ModelSerializer):
    """docstring for UserCreateSerializer."""
    email2 = EmailField(label="Confirm Email", write_only=True)
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            "email2",

        ]
        extra_kwargs = {"password": {
                        "write_only": True
                            }, "email2": {"required"}}
    def validate(self, data):
        email = data['email']
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise serializers.ValidationError("this user has already registered.")
        return data

    def validate_email2(self, value):
         data = self.get_initial()
         email1 = data.get('email')
         email2  = value
         if email1 != email2 :
            raise serializers.ValidationError("email must match")
         return value



    def create(self, validated_data):
        password = validated_data.pop('password', None)
        validated_data.pop('email2')
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class UserLoginSerializer(serializers.ModelSerializer):
    """docstring for UserCreateSerializer."""
    token = serializers.CharField(allow_blank=True, read_only = True)
    username = serializers.CharField(required=False, allow_blank=True)
    email = serializers.EmailField(label="Email Address",required=False, allow_blank=True)
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'token'

        ]
        extra_kwargs = {"password": {
                        "write_only": True
                            }, "email2": {"required"}}
    def validate(self, data):
        user_obj = None
        email = data.get('email', None)
        username = data.get('username', None)
        password = data['password']
        if not email and not username:
            raise serializers.ValidationError("A username or an email is required to login.")

        user = User.objects.filter(
                Q(email=email)
                |
                Q(username=username)
                ).distinct()
        print(user.distinct())
        user = user.exclude(email___isnull=True).exclude(email__iexact=='') #if email is empty
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise serializers.ValidationError("This username/email is not valid.")

        if user_obj:
            if not user_obj.check_password(password):
                raise serializers.ValidationError("Incorrect credentials please try again.")

        data["token"] = "some randome"
        return data
    #
    # def validate_email2(self, value):
    #      data = self.get_initial()
    #      email1 = data.get('email')
    #      email2  = value
    #      if email1 != email2 :
    #         raise serializers.ValidationError("email must match")
    #      return value



    # def create(self, validated_data):
    #     password = validated_data.pop('password', None)
    #     instance = self.Meta.model(**validated_data)
    #     if password is not None:
    #         instance.set_password(password)
    #     instance.save()
    #     return instance




class UniversitySerializer(serializers.ModelSerializer):
    #owner = serializers.Field('owner.username')
    class Meta:
        model = University
        fields = '__all__'

class StudentSerializer(serializers.ModelSerializer):
    def validate(self, data):
        print(data['first_name'])
        # if (len(data['first_name']) < 5):
        #     raise serializers.ValidationError({"first_name": "Length is not proper"})
        return data

    # def dispatch(self, *args, **kwargs):
    #     try:
    #         return super(StudentSerializer, self).dispatch(*args, **kwargs)
    #     except ParseError:
    #         print(ParseError)
    #         pass
    #         # Show an error page

    class Meta:
        model = Student
        fields = '__all__'
            #['first_name', 'last_name', 'university', 'mobile']
        #read_only_fields = ['slug']




    # def __init__(self, data, *args, **kwargs):
    #
    #     if not data:  # data is empty, meaning a fresh object
    #         super(StudentSerializer, self).__init__(*args, **kwargs)
    #
    #     else:
    #         # Retrieving the form's information
    #         self.first_name = data['first_name']
    #         self.last_name = data['last_name']
    #         self.university = data['university']
    #         self.mobile = data['mobile']
    #
    #         super(StudentSerializer, self).__init__(*args, **kwargs)
    # #
    # def create(self, validated_data):
    #     return StudentSerializer(**validated_data)
    #
    # def post(self, request):
    #     stud = StudentSerializer(data=request.data)
    #     if stud.is_valid():
    #         stud.save()



from django.conf import settings
from django.core.mail import BadHeaderError, send_mail
from django.template.loader import get_template,render_to_string



class PaymetDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment_Detail
        fields = '__all__'


    def validate(self, data):
        #print(dir(data))
        print(data['user'].id)

        return data



    def create(self, validated_data):
        print(int(validated_data['duration']) == 2)
        payment_month = {}
        if int(validated_data['duration']) == 1: #month
            dur_month = date.today() + relativedelta(months=+1)
        elif int(validated_data['duration']) == 2: #yearly
            print('yearly')
            dur_month = date.today() + relativedelta(years=+1)

            for x in reversed(range(1, 12)):
                payment_month['p'+ str(x+1)] = date.today() + relativedelta(months=+x)
                #print(x)
            #print(payment_month)

        cus = Payment_Detail.objects.filter(user_id=validated_data['user'].id).filter(maintain_status='continue').order_by('-created_at')
        print(cus)
        #marks = [field.values() == None for field in cus.values() ]
        pioneer_member = {}
        pioneer_member['expiry_date'] = dur_month
        if cus.exists():
            marks = [field.items() for field in cus.values()]
            print('p12')
            print(cus)

            marks1 = [(key+'-'+str(value)) for (key, value) in marks[0] if value == None]
            #print(marks1)
            #print((re.search(r'\d+', marks1[0])).group())
            try:
                gotdata = int(re.search(r'\d+', marks1[0]).group())
            except IndexError:
                gotdata = 12
            print( gotdata == 12)
            if bool(marks1) == False:
                print("exist but continueb but it should completed")
                payment_month['user_id'] = validated_data['user'].id
                pioneer_member['subscribed_date'] = date.today()
                payment_month['p1'] = date.today()
                cus.update(maintain_status='completed')
                payment_month['maintain_status'] =  'completed' if (int(validated_data['duration']) == 2) else 'continue'
                Payment_Detail.objects.create(**payment_month)
            else:
                if int(validated_data['duration']) == 1:
                    try:
                        month_digit = int(re.search(r'\d+', marks1[0]).group())
                    except IndexError:
                        month_digit = None
                    #month_digit = gotdata
                    print('as'+str(month_digit))
                    # print('next_date : '+ cus.values()[0]['p'+str(month_digit+1)])
                    if month_digit < 12: #11 <12 or 10 <11
                        print("inside month but less than 12")
                        print(cus.values()[0]['p' + str(month_digit+1)])
                        if cus.values()[0]['p' + str(month_digit+1)] != None:
                            print("inside month but less than 12 and last col")
                            #int(re.search(r'\d+', marks1[1]).group()):
                            pioneer_member['expiry_date'] = cus.values()[0]['p12'] + relativedelta(months=+1)
                            payment_month['maintain_status'] =  'completed'
                        print('hi outside')
                        payment_month['p'+str(month_digit)] = date.today()
                        cus.update(**payment_month)
                    elif month_digit == 12:
                        print("inside month none")

                        pioneer_member['expiry_date'] = date.today()  + relativedelta(months=+1)
                        # cus.values()[0]['p12'] if cus.values()[0]['p12'] != None else date.today()
                        payment_month['maintain_status'] =  'completed'
                        payment_month['p12'] =  date.today()
                        cus.update(**payment_month)
                elif int(validated_data['duration']) == 2:
                    print("inside year but not new connection")
                    pioneer_member['subscribed_date'] = date.today()
                    payment_month['p1'] = date.today()
                    payment_month['maintain_status'] =  'completed'
                    payment_month['user_id'] = validated_data['user'].id
                    cus.update(maintain_status='completed')
                    # Payment_Detail.objects.create(**payment_month)
                    Payment_Detail(**payment_month).save()
            print("nothing happened")
        else:
            print("new connection")
            pioneer_member['subscribed_date'] = date.today()
            payment_month['p1'] = date.today()
            payment_month['maintain_status'] =  'completed' if (int(validated_data['duration']) == 2) else 'continue'
            payment_month['user_id'] = validated_data['user'].id
            Payment_Detail.objects.create(**payment_month)

        #print([ (key+'-'+str(value)) for (key, value) in marks[0].items() if value != None ])
        #print([field.name for field in cus.model._meta.get_fields()])
        # for item in marks:
        #     print(item.keys())
        #print(len(cus.first()))
        #print(next((x for x in cus.values() if None in x), None))
        # for x in cus.model._meta.local_fields:
        #     print (x)

        #print(filter(lambda cus.model._meta.fields:cus.model._meta.fields==None,cus.values()))
        # print(dir(cus.model._meta.fields))
        # print(cus.model._meta.fields.__getitem__)
        # for field in cus.model._meta.fields:
        #     print field.name
        #if cus.exists():
        print('pioneer member')
        print(pioneer_member)
        #print(datetime.now()+ relativedelta(day=1))
        print('end')
        #return Payment_Detail(**validated_data)
        pass
        # subject = 'payment is done'
        # from_email = settings.DEFAULT_FROM_EMAIL
        # to_email = ['varunajmera0@gmail.com']
        # # contact_message = "hey received! mailed"
        # contact_message1 = get_template('form_template.html').render({'varun': 'ajmera'})
        # contact_message = render_to_string('form_template.html', {'varun': 'ajmera'})
        # send_mail(subject, contact_message1, from_email, to_email, fail_silently=True, html_message=contact_message)
        pioneer_member['user_id'] = validated_data['user'].id
        pioneer_member['pioneer_type'] = 'monthly'
        pioneer_member['payment_method'] = 'link'
        pioneer_member['initial_amount'] = 500
        pioneer_member['subscription_id'] = 'sub_asda12xsrr'
        pioneer_member['membership_code'] = '1800672168002'
        Membership(**pioneer_member).save()
        return validated_data
