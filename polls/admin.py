from django.contrib import admin
from django.contrib.auth import get_user_model
from .forms import UserAdminCreationForm, UserAdminChangeForm
# Register your models here.
from .models import University, Student, Story, FeatureStory, InfographicStory, GalleryStory, ManagementLog
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

User = get_user_model()


class UserAdmin(BaseUserAdmin):
        # The forms to add and change user instances
    form = UserAdminChangeForm #edit view
    add_form = UserAdminCreationForm #create view

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'admin')
    list_filter = ('admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('full_name',)}),
        ('Permissions', {'fields': ('admin','staff',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email', 'full_name')
    ordering = ('email',)
    filter_horizontal = ()



class ManagementAdmin(admin.ModelAdmin):
    list_display = ['task', 'count', 'started', 'finished', 'state']

admin.site.register(User, UserAdmin)
admin.site.register(University)
admin.site.register(Student)
admin.site.register(Story)
admin.site.register(FeatureStory)
admin.site.register(InfographicStory)
admin.site.register(GalleryStory)
admin.site.register(ManagementLog, ManagementAdmin)
# Remove Group Model from admin. We're not using it.
admin.site.unregister(Group)
