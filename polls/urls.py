from django.conf.urls import url
from rest_framework import routers
from .views import StudentViewSet, UniversityViewSet, UserCreateAPIView, UserLoginAPIView, LoginView, LogoutView, PaymentDetailViewSet
from django.contrib.auth.models import User
from rest_framework_jwt.views import obtain_jwt_token


router = routers.DefaultRouter()
router.register(r'students', StudentViewSet, base_name='Student')
router.register(r'universities', UniversityViewSet)
router.register(r'paym', PaymentDetailViewSet)
#router.register(r'user', UserCreateAPIView.as_view(queryset=User.objects.all(), serializer_class=UserCreateAPIView), name='user')

urlpatterns = [
    url(r'^auth/token/$', obtain_jwt_token),
    url(r'^login/$', UserLoginAPIView.as_view(), name='login'), #normal login
    url(r'^register/$', UserCreateAPIView.as_view(), name='register'),
    url(r'^signin/$', LoginView.as_view(), name='signin'), #using authtoken django
    url(r'^logout/$', LogoutView.as_view(), name='logout'), #using authtoken django

]
urlpatterns += router.urls
