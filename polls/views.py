# from demo.serializers import DemoSerializer
# from rest_framework import generics
# from rest_framework.permissions import AllowAny
# from demo.models import Person
# from django.http import HttpResponse
#
# # Create your views here.
# class UserList(generics.ListCreateAPIView):
#     queryset = Person.objects.all()
#     serializer_class = DemoSerializer
#     #permission_classes = (AllowAny)
#
#     def list(self, request):
#         # Note the use of `get_queryset()` instead of `self.queryset`
#         queryset = self.get_queryset()
#         serializer = DemoSerializer(queryset, many=True)
#         return HttpResponse(serializer.data)

from rest_framework import viewsets, permissions, generics, views
from .models import University, Student, Payment_Subscription, Payment_Detail
from .serializers import UniversitySerializer, StudentSerializer, UserCreateSerializer, UserLoginSerializer, LoginSerializer, PaymetDetailSerializer
from rest_framework.response import Response
from django.shortcuts import get_object_or_404, render
from django.views import View
import razorpay
from django.http import JsonResponse
from .permissions import IsOwnerOrReadOnly
from django.contrib.auth import get_user_model
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from django.contrib.auth import login as django_login, logout as django_logout
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication, SessionAuthentication, BasicAuthentication

User = get_user_model()



class UserLoginAPIView(APIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        print(serializer);
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)



class UserCreateAPIView(generics.ListCreateAPIView):
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()


class UniversityMixin():
    #permission_classes = [IsOwnerOrReadOnly,]

    def pre_save(self, obj):
        obj.owner = self.request.user

class PaymentDetailViewSet(viewsets.ModelViewSet):
    #permission_classes = [permissions.AllowAny]
    queryset = Payment_Detail.objects.all()
    serializer_class = PaymetDetailSerializer

class StudentViewSet(viewsets.ViewSet):
    # queryset = Student.objects.all()
    # serializer_class = StudentSerializer

    def list(self, request):
        queryset = Student.objects.all()
        serializer = StudentSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Student.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = StudentSerializer(user)
        return Response(serializer.data)


class UniversityViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly]
    authentication_classes = [TokenAuthentication, SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]
    queryset = University.objects.all()
    serializer_class = UniversitySerializer


class PaymentView(View):
    form_class = ''
    initial = {'key': 'value'}
    #template_name = 'form_template.html'
    def get(self, request, *args, **kwargs):
        return render(request, template_name = 'form_template.html')


class RazorpayView(View):
    def post(self, request, *args, **kwargs):
        client = razorpay.Client(auth=("rzp_test_UCyDCqZLJS0p4b", "zMlDpfM22D1ciamZ89snOzyy"))
        data = {
            'plan_id': 'plan_ALzW7hMS7YBqgs',
            'total_count': 12,
            'customer_notify': 1
        }
        sub = client.subscription.create(data=data)

        # if form.is_valid():
        #     # <process form cleaned data>
        #     return HttpResponseRedirect('/success/')

        return JsonResponse({
            'sub': sub,
            'amount': 50000
        })

class PayView(View):
    """docstring for ."""
    def post(self, request, *args, **kwargs):
        client = razorpay.Client(auth=("rzp_test_UCyDCqZLJS0p4b", "zMlDpfM22D1ciamZ89snOzyy"))
        if request.POST.get('subscription_id'):
            sub = client.subscription.fetch(request.POST.get('subscription_id'))
            print(request)
            print(sub)
            print(sub['status'])
            data = {
                'user': 1,
                'plan_id': request.POST.get('plan_id'),
                'subscription_id':request.POST.get('subscription_id'),
                'role': 'pioneer',
                'status': sub['status'],
                'amount': 500,
                'paid_count': sub['paid_count']
            }
            pay = Payment_Subscription(user=1, plan_id=request.POST.get('payment_id'),
            subscription_id=request.POST.get('subscription_id'),
            role='pioneer',status=sub['status'], amount=500, paid_count=sub['paid_count'])

            pay.save()

            # if form.is_valid():
            #     # <process form cleaned data>
            #     return HttpResponseRedirect('/success/')

            return JsonResponse({
                'success': 'saved'
            })



class LoginView(views.APIView):
    permission_classes = [permissions.AllowAny]
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        django_login(request, user)
        token, created = Token.objects.get_or_create(user=user)
        return Response({"token" : token.key}, status = 200)

class LogoutView(views.APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = (TokenAuthentication, )

    def post(self, request):
        django_logout(request)
        return Response(status=204)
