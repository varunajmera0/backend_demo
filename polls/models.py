from django.db import models
from django.db.models.signals import pre_save, post_save
from django.utils.text import slugify
from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from django.contrib.auth.models import ( AbstractBaseUser, BaseUserManager )
from django.conf import settings
from django.contrib.auth import get_user_model




class ManagementLog(models.Model):
    task = models.CharField(max_length=120)
    count = models.IntegerField(blank=True, null=True)
    started = models.DateTimeField(blank=True, null=True)
    finished = models.DateTimeField(blank=True, null=True)
    state_choices = (
        (1, "Starting"),
        (2, "In Progress"),
        (3, "Finished"),
    )
    state = models.IntegerField(choices=state_choices, blank=True, null=True)


STORY_TYPES = (
    ('f', 'Feature'),
    ('i', 'Infographic'),
    ('g', 'Gallery'),
)

class Story(models.Model):
    type = models.CharField(max_length=1, choices=STORY_TYPES)
    title = models.CharField(max_length=100)
    body = models.TextField(blank=True, null=True)
    infographic = models.ImageField(blank=True, null=True)
    link = models.URLField(blank=True, null=True)
    gallery = models.CharField(max_length=255,blank=True, null=True)

class FeatureManager(models.Manager):
    def get_queryset(self):
        return super(FeatureManager, self).get_queryset().filter(
            type='f')

class InfographicManager(models.Manager):
    def get_queryset(self):
        return super(InfographicManager, self).get_queryset().filter(
            type='i')

class GalleryManager(models.Manager):
    def get_queryset(self):
        return super(GalleryManager, self).get_queryset().filter(
            type='g')

class FeatureStory(Story):
    objects = FeatureManager()
    class Meta:
        proxy = True

class InfographicStory(Story):
    objects = InfographicManager()
    class Meta:
        proxy = True

class GalleryStory(Story):
    objects = GalleryManager()
    class Meta:
        proxy = True



# Create your models here.

# class Person(models.Model):
#     first_name = models.CharField(max_length=30)
#     last_name = models.CharField(max_length=30)
# print(get_user_model())
# User = get_user_model()
def last_name_validate(value):
    if len(value) < 3:
        raise serializers.ValidationError('Length')

class University(models.Model):
    print(settings.AUTH_USER_MODEL)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='universities', on_delete=models.CASCADE, )
    #owner = models.ForeignKey('auth.User', on_delete=models.CASCADE,)
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "University"
        verbose_name_plural = "Universities"

    def __unicode__(self):
        return self.name

class Student(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, validators=[last_name_validate])
    mobile = models.CharField(max_length=50)
    slug = models.SlugField(null=True, blank=True, error_messages={"does_not_exist": "Some custom error message."})
    university = models.ForeignKey(University, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Students"

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def validate(self, data):
        print('ajmeri')
        print(data)

    # def save(self, *args, **kwargs):
    #     return super(Student, self).save(*args, **kwargs)
    def save(self, *args, **kwargs):
        super(Student, self).save(*args, **kwargs)
        print(self.first_name)
        print('varun')



    # def clean(self):
    #     if len(self.last_name) < 3:
    #         raise serializers.ValidationError({'firstName': 'Length'})


# def save_slug(sender, instance, *args, **kwargs):
#     print('pre save')
#     if not instance.slug:
#         if len(instance.first_name) < 7:
#             # print("error")
#             raise serializers.ValidationError({"FIRSTNAME": 'firstname should be more than 7'})
#         instance.slug = slugify(instance.first_name + instance.last_name)
#
#
# pre_save.connect(save_slug,sender=Student)

def post_save_slug(sender, instance, created, *args, **kwargs):
    # print('post save')
    # print(instance.first_name)
    # print(dir(instance))
    # print('varunajmera')
    # print(kwargs['signal'])
    # print(kwargs)
    # print(instance)
    # print(instance.date_error_message)
    if created:
        if not instance.slug:
            ob = Student.objects.get(pk=instance.id)
            if len(instance.first_name) < 7:
                print("error")
                # raise serializers.ValidationError({
                #     "FIRSTNAME": 'fn is not correct',
                #     "first_name": instance.first_name
                # })

                # kwargs['update_fields'] = Student.save(instance);
                # raise serializers.ValidationError({'fn': 'fn error'})
                #instance.date_error_message = 'chutiya'
                #print(instance.date_error_message)
                # return ({
                #     instance.date_error_message : 'chu',
                #     #instance: instance
                # })
                #raise serializers.ValidationError({"does_not_exist":   'fn is not correct'})
                #print(serializers)
                # return ({
                #     "_errors":  "asd bhag bc",
                #     "invalid":  'fn is not correct',
                #     "error_messages['first_name']":  'fn is not correct',
                #     "non_field_errors":  {'first_name': 'fn is not correct'},
                #     "NON_FIELD_ERRORS_KEY":   'fn is not correct',
                #     "does_not_exist":   'fn is not correct',
                #     "instance": instance
                #     })




            instance.slug = slugify(instance.first_name + instance.last_name)

            #Student.objects.create(instance)
    #else:
        #instance.student.save()

post_save.connect(post_save_slug, sender=Student)

class Payment_Detail(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='payment_detail', on_delete=models.CASCADE, )
    p1 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p2= models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p3 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p4 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p5 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p6 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p7 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p8 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p9 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p10 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p11 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    p12 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    duration = models.CharField(max_length=12, null=True, blank=True)
    maintain_status = models.CharField(max_length=12, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Payment_Subscription(models.Model):
    user = models.IntegerField()
    subscription_id = models.CharField(max_length=20,null=True, blank=True)
    plan_id = models.CharField(max_length=20,null=True, blank=True)
    status = models.CharField(max_length=12,null=True, blank=True)
    type = models.CharField(max_length=10,null=True, blank=True)
    role = models.CharField(max_length=20,null=True, blank=True)
    amount = models.FloatField(null=True, blank=True)
    paid_count = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

choices = (
('link','Link'),
('cash', 'CASH')
)

choices_pioneer = (
('monthly','Monthly'),
('yearly', 'Yearly')
)

class Membership(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="memberships", on_delete=models.CASCADE,)
    membership_code = models.CharField(max_length=20, null=True, blank=True)
    subscription_id = models.CharField(max_length=20, null=True, blank=True)
    subscribed_date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    expiry_date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    offer = models.IntegerField(null=True, blank=True)
    initial_amount = models.FloatField(null=True, blank=True)
    payment_method = models.CharField(max_length=5,choices=choices)
    pioneer_type = models.CharField(max_length=10,choices=choices_pioneer)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table="membership"


class UserManager(BaseUserManager):
    #use_in_migrations = True
    def create_user(self, email, full_name=None, password=None, is_active= True, is_staff=False, is_admin=False): # if REQUIRED_FIELD has any field like full_name then we have to write
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have password ")
        if not full_name:
            raise ValueError("Users must have full name ")
        print(full_name)
        user = self.model(
            email = self.normalize_email(email),
            full_name=full_name,
        )
        user.set_password(password)
        user.is_active = is_active
        user.is_staff = is_staff
        user.is_admin = is_admin

        user.save(using=self._db)
        return user

    def create_staffuser(self, email, full_name, password=None):
        user = self.create_user(
            email,
            password=password,
            full_name=full_name,
            is_staff = True
        )
        return user

    def create_superuser(self, email, full_name, password=None):
        user = self.create_user(
            email,
            full_name=full_name,
            password=password,
            is_staff = True,
            is_admin=True
        )
        return user

class CustomUser(AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True)
    username = models.CharField(max_length=255, unique=True,blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    active =  models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name'] #full_name

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        if self.full_name:
            return self.full_name
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm,obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    #@property
    def is_staff(self):
        return self.staff

    #@property
    def is_admin(self):
        return self.admin
