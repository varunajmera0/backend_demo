from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from polls.models import *
from polls.models import ManagementLog
from polls.polls_classes.management_log import Management
import time
from datetime import date
from dateutil.relativedelta import relativedelta
import re


class Command(BaseCommand):
    title = "Process Emails"
    help = 'Runs payment detail'

    log = Management(title)

    def add_arguments(self, parser):
        parser.add_argument('--id')
        parser.add_argument('--duration')

    def handle(self, *args, **options):
        self.log.in_progress()
        userid = options['id']
        duration = options['duration']
        print(int(duration) == 2)
        payment_month = {}
        if int(duration) == 1: #month
            dur_month = date.today() + relativedelta(months=+1)
        elif int(duration) == 2: #yearly
            print('yearly')
            dur_month = date.today() + relativedelta(years=+1)

            for x in reversed(range(1, 12)):
                payment_month['p'+ str(x+1)] = date.today() + relativedelta(months=+x)
                #print(x)
            #print(payment_month)

        cus = Payment_Detail.objects.filter(user_id=userid).filter(maintain_status='continue').order_by('-created_at')
        print(cus)
        #marks = [field.values() == None for field in cus.values() ]
        pioneer_member = {}
        pioneer_member['expiry_date'] = dur_month
        if cus.exists():
            marks = [field.items() for field in cus.values()]
            print('p12')
            print(cus)

            marks1 = [(key+'-'+str(value)) for (key, value) in marks[0] if value == None]
            #print(marks1)
            #print((re.search(r'\d+', marks1[0])).group())
            try:
                gotdata = int(re.search(r'\d+', marks1[0]).group())
            except IndexError:
                gotdata = 12
            print( gotdata == 12)
            if bool(marks1) == False:
                print("exist but continueb but it should completed")
                payment_month['user_id'] = userid
                pioneer_member['subscribed_date'] = date.today()
                payment_month['p1'] = date.today()
                cus.update(maintain_status='completed')
                payment_month['maintain_status'] =  'completed' if (int(duration) == 2) else 'continue'
                Payment_Detail.objects.create(**payment_month)
            else:
                if int(duration) == 1:
                    try:
                        month_digit = int(re.search(r'\d+', marks1[0]).group())
                    except IndexError:
                        month_digit = None
                    #month_digit = gotdata
                    print('as'+str(month_digit))
                    # print('next_date : '+ cus.values()[0]['p'+str(month_digit+1)])
                    if month_digit < 12: #11 <12 or 10 <11
                        print("inside month but less than 12")
                        print(cus.values()[0]['p' + str(month_digit+1)])
                        if cus.values()[0]['p' + str(month_digit+1)] != None:
                            print("inside month but less than 12 and last col")
                            #int(re.search(r'\d+', marks1[1]).group()):
                            pioneer_member['expiry_date'] = cus.values()[0]['p12'] + relativedelta(months=+1)
                            payment_month['maintain_status'] =  'completed'
                        print('hi outside')
                        payment_month['p'+str(month_digit)] = date.today()
                        cus.update(**payment_month)
                    elif month_digit == 12:
                        print("inside month none")

                        pioneer_member['expiry_date'] = date.today()  + relativedelta(months=+1)
                        # cus.values()[0]['p12'] if cus.values()[0]['p12'] != None else date.today()
                        payment_month['maintain_status'] =  'completed'
                        payment_month['p12'] =  date.today()
                        cus.update(**payment_month)
                elif int(duration) == 2:
                    print("inside year but not new connection")
                    pioneer_member['subscribed_date'] = date.today()
                    payment_month['p1'] = date.today()
                    payment_month['maintain_status'] =  'completed'
                    payment_month['user_id'] = userid
                    cus.update(maintain_status='completed')
                    # Payment_Detail.objects.create(**payment_month)
                    Payment_Detail(**payment_month).save()
            print("nothing happened")
        else:
            print("new connection")
            pioneer_member['subscribed_date'] = date.today()
            payment_month['p1'] = date.today()
            payment_month['maintain_status'] =  'completed' if (int(duration) == 2) else 'continue'
            payment_month['user_id'] = userid
            Payment_Detail.objects.create(**payment_month)

            #print([ (key+'-'+str(value)) for (key, value) in marks[0].items() if value != None ])
            #print([field.name for field in cus.model._meta.get_fields()])
            # for item in marks:
            #     print(item.keys())
            #print(len(cus.first()))
            #print(next((x for x in cus.values() if None in x), None))
            # for x in cus.model._meta.local_fields:
            #     print (x)

            #print(filter(lambda cus.model._meta.fields:cus.model._meta.fields==None,cus.values()))
            # print(dir(cus.model._meta.fields))
            # print(cus.model._meta.fields.__getitem__)
            # for field in cus.model._meta.fields:
            #     print field.name
            #if cus.exists():
            #print('pioneer member')
            #print(pioneer_member)
            #print(datetime.now()+ relativedelta(day=1))
            #print('end')
            #return Payment_Detail(**validated_data)
            #pass
            # subject = 'payment is done'
            # from_email = settings.DEFAULT_FROM_EMAIL
            # to_email = ['varunajmera0@gmail.com']
            # # contact_message = "hey received! mailed"
            # contact_message1 = get_template('form_template.html').render({'varun': 'ajmera'})
            # contact_message = render_to_string('form_template.html', {'varun': 'ajmera'})
            # send_mail(subject, contact_message1, from_email, to_email, fail_silently=True, html_message=contact_message)
            pioneer_member['user_id'] = userid
            pioneer_member['pioneer_type'] = 'monthly'
            pioneer_member['payment_method'] = 'link'
            pioneer_member['initial_amount'] = 500
            pioneer_member['subscription_id'] = 'sub_asda12xsrr'
            pioneer_member['membership_code'] = '1800672168002'
            Membership(**pioneer_member).save()
        self.log.update_count(1)
            # return validated_data
        self.log.completed()
